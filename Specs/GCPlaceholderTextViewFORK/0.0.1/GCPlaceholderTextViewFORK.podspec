Pod::Spec.new do |s|
  s.name         = "GCPlaceholderTextViewFORK"
  s.version      = "0.0.1"
  s.summary      = "A subclass of UITextView that allow a placeholder."
  s.homepage     = "https://github.com/aybar/GCPlaceholderTextView"
  s.license      = 'MIT'
  s.author       = { 'Guillaume Campagna' => 'http://gcamp.ca' }
  s.source       = { :git => "https://github.com/aybar/GCPlaceholderTextView.git", :commit => "db108221922999c91bb8941e0b88458def221d85" }
  s.platform     = :ios
  s.source_files = 'GCPlaceholderTextView/GCPlaceholderTextView.{h,m}'
  s.framework  = 'UIKit'
end