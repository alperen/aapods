Pod::Spec.new do |s|
  s.name     = 'ABBMobileBranding'
  s.version  = '1.0.4'
  s.license  = { :type => 'Apache License, Version 2.0', :text => <<-LICENSE
    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
    LICENSE
  }
  s.summary  = 'ABBMobileBranding'
  s.homepage = 'http://www.abb.com/'
  s.author   = { 'Alperen Aybar' => 'alperen.aybar@pl.abb.com' }
  s.source   = { :git => 'http://buildserver:222238641@pllod-s-isdc101.pl.abb.com:8080/scm/git/Public/ABBMobileBranding', :tag => '1.0.4' }
  s.platform = :ios, '8.0'
  s.requires_arc = true
  s.source_files = 'ABBMobileBranding/*.h'
  s.resources = 'ABBMobileBranding/Resources/*.ttf'
  s.prepare_command = 'ruby scripts/font_installer.rb'

end